class Segment{
 PVector a;
 PVector b;
 float angle;
 float size;
 
 PVector target = new PVector();
 
  Segment(float x, float y, float ang, float len){
    a = new PVector(x,y);
    b = new PVector();
    angle = ang;
    size = len;
  }
  
  public void track(Segment s){
    track(s.a.x, s.a.y);
  }
  
  public void track(float x, float y){
    angle = target.set(x,y).sub(a).heading();
    
    target.setMag(size).mult(-1);
    a.set(x,y).add(target);
  }
  
  public void update(){
    
   //Calculate B
   float dx = size * cos(angle);
   float dy = size * sin(angle);
   b.set(a).add(dx,dy);
  }
  
  public void render(){
   stroke(255);
   strokeWeight(4);
   line(a.x, a.y, b.x, b.y);
  }
  
}
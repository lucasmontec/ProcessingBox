static class InverseKinematics{
  
  private static Segment head;
  private static Segment tail;
  
  private static PVector fixedBase = new PVector();
  
  private static PVector fromTailToBase = new PVector();
  
  public static void fixAt(float x, float y){
    fixedBase.set(x,y);
  }
  
  public static void solve(Segment[] segments, float x, float y){
    head = segments[segments.length-1];
    tail = segments[0];
    
    head.track(x, y);
    head.update();
  
    for(int i=segments.length-2;i>-1;i--){
      segments[i].track(segments[i+1]);
      segments[i].update();
    }
    
    fromTailToBase.set(fixedBase).sub(tail.a);
    
    for(Segment seg : segments){
      seg.a.add(fromTailToBase);
      seg.b.add(fromTailToBase);
      seg.render();
    }
  }
  
}
Segment[] segs;
int segcount = 40;

void setup(){
 size(800,600);
 
 segs = new Segment[segcount];
 for(int i=0;i<segcount;i++){
   segs[i] = new Segment(width/2, height/2, 0, 12);
 }
 
 InverseKinematics.fixAt(width/2, height);
}

void draw(){
  background(100);
  
  InverseKinematics.solve(segs, mouseX, mouseY);
}
Open Processing Sketches
========================

Here I store some of my sketches. You can use them, download them, modify, sell, buy, copy, derivate, have fun. If you want to publish sketches just send a merge request. <3
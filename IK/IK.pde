
Segment[] segs;
Segment first;

int segcount = 40;

PVector lastMousePos = new PVector();

color newColor = color(random(255),random(255),random(255));
color target = color(255);
float reach = 0;
float lastTime = 0;

void setup(){
 size(800,600);
 
 segs = new Segment[segcount];
 for(int i=0;i<segcount;i++){
   segs[i] = new Segment(width/2, height/2, 0, 12);
 }
 
 first = segs[segcount-1];
 lastMousePos.set(mouseX, mouseY);
 lastTime = millis();
}

void draw(){
  lastTime = millis() - lastTime;
  background(100);
  lastMousePos.sub(mouseX, mouseY);
  
  first.track(mouseX, mouseY);
  first.update();
  first.render();
  
  first.thickness = 2f + 4f*(1f+sin(millis()/500f)) + lastMousePos.mag();
  updateTargetColor(lastMousePos.mag(), lastTime);
  first.col = target;
  
  for(int i=segcount-2;i>-1;i--){
    segs[i].track(segs[i+1]);
    segs[i].update();
    segs[i].render();
  }
  
  lastMousePos.set(mouseX, mouseY);

  lastTime = millis();
}

void updateTargetColor(float mag, float dt){
  reach += dt*(mag/2);
  reach = constrain(reach, 0, 1000);
  
  target = lerpColor(target, newColor, reach/1000f);
  
  if(reach == 1000){
    newColor = color(random(255),random(255),random(255));
    reach = 0;
  }
}
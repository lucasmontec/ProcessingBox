static class EntityManager {
  
  private static ArrayList<GameObject> entities = new ArrayList<GameObject>();
  
  private static ArrayList<GameObject> addEnts = new ArrayList<GameObject>();
  private static ArrayList<GameObject> rmEnts = new ArrayList<GameObject>();
  
  public static boolean contains(GameObject go){
    return entities.contains(go);
  }
  
  public static void add(GameObject go){
    if(!entities.contains(go)){
     addEnts.add(go);
     go.innerAwake();
    }
  }
  
  public static <T extends GameObject> ArrayList<T>  get(Class<T> cla){
    ArrayList<T> ret = new ArrayList<T>();
    
    for(GameObject go : entities){
      if(go.getClass().equals(cla)){
        ret.add((T)go);
      }
    }
    
    return ret;
  }
  
  public synchronized static void remove(GameObject go){
    rmEnts.add(go); 
  }
  
  public synchronized static void update(float dt){
   
    for(GameObject go : entities){
      if(!go.shouldDestroy()){
        go.update(dt);
      }else{
        rmEnts.add(go); 
      }
    }
    
    //Remove to destroy
    entities.removeAll(rmEnts);
    rmEnts.clear();
    
    //Add new ents
    entities.addAll(addEnts);
    addEnts.clear();
    
  }
  
  private static ArrayList<GameObject> copy(ArrayList<GameObject> list){
    return new ArrayList<GameObject>(list); 
  }
  
  public synchronized static void render(){
    for(GameObject go : entities){
      go.render();
    }
  }
  
  public static int count(){
    return entities.size(); 
  }
}
class Tentacle extends GameObject{
  private final boolean debug = false;
  
  Segment[] segs;
  int segcount = 40;
  float seglength = 12;
  
  private Segment head;
  
  PVector startPosition = new PVector();
  PVector targetPosition = new PVector();
  
  Timer tentacleTick = new Timer();
  
  Item target;
  
  boolean grabbed = false;
  
  //Visuals
  color newColor = color(random(255),random(255),random(255));
  color targetColor = color(255);
  PVector lastPos = new PVector();
  float colorReach = 0;
  
  //Spline setup
  float movementTime = 2;
  float movementTimer = 0;
  PVector movementStart = new PVector();
  PVector movementCenter = new PVector();
  PVector movementEnd = new PVector();
  PVector movementPos = new PVector();
  
  public Tentacle(float sx, float sy, int segCount){
    this(sx, sy, segCount, 18);
  }
  
  public Tentacle(float sx, float sy){
    this(sx, sy, 40, 18);
  }
  
  public Tentacle(float sx, float sy, int segCount, float segLength){
    startPosition.set(sx, sy);
    segcount = segCount;
    seglength = segLength;
  }
  
  public void awake(){
   segs = new Segment[segcount];
   for(int i=0;i<segcount;i++){
     segs[i] = new Segment(startPosition.x, startPosition.y, 0, seglength);
   }
   head = segs[segs.length-1];
  }
  
  private void setupSpline(PVector start, PVector target){
    movementTime = random(1000, 3000);
    movementTimer = 0;
    movementStart.set(start);
    movementPos.set(start);
    movementEnd.set(target);
    movementCenter.set(width/2, height/2).add(random(width*-0.1f, width*0.1f), random(height*-0.1f, height*0.1f));
  }
  
  private void updateSpline(float dt){
    movementTimer += dt;
    float passed = min(1f, movementTimer/movementTime);
    movementPos.set(
    lerp(
    lerp(movementStart.x, movementCenter.x, passed),
    lerp(movementCenter.x, movementEnd.x, passed),
    passed),
    lerp(
    lerp(movementStart.y, movementCenter.y, passed),
    lerp(movementCenter.y, movementEnd.y, passed),
    passed)
    );
  }
  
  private synchronized void selectItem(Item i){
    if(!i.selected){
      i.selected = true;
      target = i;
      EntityManager.remove(target);
      setupSpline(head.a, target.position);
    }
  }
  
  private void updateTargetColor(float mag, float dt){
    colorReach += dt*(mag/2);
    colorReach = constrain(colorReach, 0, 1000);
    
    targetColor = lerpColor(targetColor, newColor, colorReach/1000f);
    
    if(colorReach == 1000){
      newColor = color(random(255),random(255),random(255));
      colorReach = 0;
    }
  }
  
  public void update(float dt){
    lastPos.set(head.a);
    
    InverseKinematics.solve(segs, startPosition, targetPosition);
    
    lastPos.sub(head.a);
    updateTargetColor(lastPos.mag(), dt);
    head.col = targetColor;
    head.thickness = 2f + 4f*(1f+sin(millis()/500f)) + lastPos.mag();
    
    if(tentacleTick.finished(500)){
      
      //If no target, get the first waiting
      if(target == null){
        
        ArrayList<Item> items = EntityManager.get(Item.class);
        //Find first wating item
        for(int i=0;i<items.size();i++){
          //Get the item
          if(items.get(i).isWaiting()){
            selectItem(items.get(i));
            break;
          }
        }
        
      }     
    }
    
    if(target != null){
      target.update(dt);
      if(splineDone() && !grabbed){
        grabbed = true;
        //Begin next movement
        setupSpline(target.position, target.position.copy().add(width*0.6f,0));
      }
      
      //Holding target
      if(grabbed){
        target.position.set(head.b);
        
        //When reached target, release
        if(splineDone()){
          EntityManager.add(target);
          target = null;
          grabbed = false;
          setupSpline(head.a, new PVector(random(width*0.2f, width*0.8f), random(height*0.2f, height*0.8f)));
        }
      }
    }else{
      if(splineDone()){
        setupSpline(head.a, new PVector(random(width*0.2f, width*0.8f), random(height*0.2f, height*0.8f)));
      }
    }
    
    targetPosition.set(movementPos);
    updateSpline(dt);
  }
  
  private boolean splineDone(){
    return movementTimer >= movementTime;
  }
  
  public void render(){
    for(Segment seg : segs){
      seg.render();
    }
    
    if(target != null){
      target.render();
    }
    
    if(debug){
      stroke(0,255,0);
      strokeWeight(1);
      line(movementStart.x, movementStart.y, movementCenter.x, movementCenter.y);
      line(movementCenter.x, movementCenter.y, movementEnd.x, movementEnd.y);
      ellipse(movementPos.x, movementPos.y, 10, 10);
    }
  }
  
  public void track(float x, float y){
    targetPosition.set(x,y);
  }
}
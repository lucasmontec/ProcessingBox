class Timer{
  
  private float start = 0;
  
  public void start(){
    start = millis();
  }
  
  public float elapsed(){
    return millis() - start;
  }
  
  public boolean finished(float duration){
    return elapsed() >= duration;
  }
}
float lastTime = 0;


Timer itemspawn = new Timer();

void setup(){
  size(800,600);
  EntityManager.add(new Tentacle(width/2, 0) );
  EntityManager.add(new Tentacle(width/2, 0) );
  EntityManager.add(new Tentacle(width/2, 0) );
  EntityManager.add(new Tentacle(width/2, 0) );
  EntityManager.add(new Tentacle(width/2, 0) );
  
  itemspawn.start();
}

void draw(){
  lastTime = millis() - lastTime;
  background(60);
  
  if(itemspawn.finished(1000) && EntityManager.count() < 15){
    itemspawn.start();
    EntityManager.add(new Item(-10, height*(0.8f+random(-0.2,0.15)), 50));
  }
  
  EntityManager.update(lastTime);
  EntityManager.render();
  
  lastTime = millis();
}
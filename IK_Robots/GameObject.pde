abstract class GameObject{
  
  private boolean destroy = false;
  private boolean awoke = false;
  
  public void innerAwake(){
    if(!awoke){
      awake(); 
    }
    awoke = true;
  }
  
  public abstract void awake();
  public abstract void render();
  public abstract void update(float dt);
  
  public void destroy(){
    destroy = true;
  }
  
  public boolean shouldDestroy(){
    return destroy;
  }
  
}
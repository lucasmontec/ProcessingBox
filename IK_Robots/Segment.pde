class Segment {
 PVector a;
 PVector b;
 float angle;
 float size;
 
 public color col = color(255);
 public float thickness = 4;
 
 PVector target = new PVector();
 
  Segment(float x, float y, float ang, float len){
    a = new PVector(x,y);
    b = new PVector();
    angle = ang;
    size = len;
  }
  
   public void track(PVector s){
    track(s.x, s.y);
  }
  
  public void track(Segment s){
    track(s.a);
    
    thickness = lerp(thickness, s.thickness, 0.2);
    col = lerpColor(col, lerpColor(s.col, color(0), 0.05), 0.5);
  }
  
  public void track(float x, float y){
    angle = target.set(x,y).sub(a).heading();
    
    target.setMag(size).mult(-1);
    a.set(x,y).add(target);
  }
  
  public void update(){
   //Calculate B
   float dx = size * cos(angle);
   float dy = size * sin(angle);
   b.set(a).add(dx,dy);
  }
  
  public void render(){
   stroke(col);
   strokeWeight(thickness);
   line(a.x, a.y, b.x, b.y);
  }
  
}
class Item extends GameObject {
  
  int shape = 0;
  public PVector position = new PVector();
  float size;
  color col;
  boolean waiting = false;
  boolean selected = false;
  
  private float stoppingPoint = 0.15f;
  
  public Item(float sx, float sy, float size){
    position.set(sx, sy);
    col = color(random(40,180),random(60),random(80,120));
    this.size = size;
    stoppingPoint = random(0.05f, 0.15f);
  }
  
  public boolean isWaiting(){
    return waiting;
  }
  
  public void awake(){
    shape = int(random(5));
  }
  
  public void update(float dt){
    if(position.x < width*stoppingPoint || position.x >= width*0.5f){
      position.x += dt*0.1f;
    }else if(!waiting && position.x < width*0.5f){
     waiting = true; 
    }
    
    if(waiting && position.x >= width*0.5f){
      waiting = false;
    }
    
    if(position.x > width+size){
      destroy();
    }
  }
  
  public void render(){
    noStroke();
    fill(col);
    
    switch(shape){
      case 0:
        ellipse(position.x, position.y, size, size);
      break;
      case 1:
        rectMode(CENTER);
        rect(position.x, position.y, size, size, 7);
      break;
      case 2:
        rectMode(CENTER);
        rect(position.x, position.y, size/2, size, 7);
      break;
      case 3:
        rectMode(CENTER);
        rect(position.x, position.y, size, size/2, 7);
      break;
      case 4:
        rectMode(CENTER);
        rect(position.x, position.y, size/2, size/2, 7);
      break;
    }
  }
  
}
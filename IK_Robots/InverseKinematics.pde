static class InverseKinematics{
  
  private static Segment head;
  private static Segment tail;
  
  private static PVector fromTailToBase = new PVector();
  
  public static void solve(Segment[] segments, PVector start, PVector target){
    head = segments[segments.length-1];
    tail = segments[0];
    
    head.track(target);
    head.update();
  
    for(int i=segments.length-2;i>-1;i--){
      segments[i].track(segments[i+1]);
      segments[i].update();
    }
    
    fromTailToBase.set(start).sub(tail.a);
    
    for(Segment seg : segments){
      seg.a.add(fromTailToBase);
      seg.b.add(fromTailToBase);
    }
  }
  
}